import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.crypto.Data;

public class MonitoredData {
		
		
		private Date startTime;
		private Date endTime;
		private Activity activityLabel;
		private static String path="Activities.txt";
		public MonitoredData(Date startTime,Date endTime,Activity activityLabel){
			this.startTime=startTime;
			this.endTime=endTime;
			this.activityLabel=activityLabel;
		}
		/*public int getStartTime(){
			return this.startTime;
		}*/
		public void setStartTime(Date d){
			this.startTime=d;
		}
		public void setEndTime(Date d){
			this.endTime=d;
		}
		public void setActivityLabel(Activity a){
			this.activityLabel=a;
		}
		public Date getStartTime(){
			return startTime;
		}
		public Activity getActivityLabel(){
			return this.activityLabel;
		}
		public int getDay(){
			Calendar c=Calendar.getInstance();
			c.setTime(this.startTime);
			return c.get(Calendar.DAY_OF_MONTH);
			//return (this.startTime.getTime()/1000/60/60/24) % 30;
		}
		public String getActivityLabelString(){
			return this.activityLabel.toString();
		}
		public Long getDifferenceBetweenTimes(){
			return this.endTime.getTime()-this.startTime.getTime();
		}
}
