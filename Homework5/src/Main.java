import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
//import java.util.function.Predicate;
import java.util.stream.Collectors;


public class Main {
	
	public static void main(String[] args){
		String s;
		String[] ss;
	//	Date date;
		List<MonitoredData> mon= new ArrayList<MonitoredData>();
		List<String[]> sss=new ArrayList<String[]>();
		//reading provided text file and saving it into a list
		try {
			FileReader fileRead=new FileReader("Activities.txt");
			BufferedReader br = new BufferedReader(fileRead);
			s=br.readLine();
			while(s!=null){
				ss=s.split("[\t ]+");
				sss.add(ss);
				s=br.readLine();
			}
			
		} catch ( IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String[] s1=sss.get(0)[0].split("-",'\t');
		Date d1,d2;
		
		Activity a;
		//creating a list of monitored datas
		for(int i=0;i<sss.size();i++){
			s1=sss.get(0)[0].split("-");
			try {
				d1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(sss.get(i)[0] +" " + sss.get(i)[1]);
				d2=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(sss.get(i)[2] +" "+ sss.get(i)[3]);
				a=getActivity(sss.get(i)[4]);
				mon.add(new MonitoredData(d1,d2,a));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
			//TASKUL 1
			Lam lam=(x,y)->y.getTime()/1000/60/60/24-x.getTime()/1000/60/60/24;
			Long days=lam.calculateDays(mon.get(0).getStartTime(),mon.get(mon.size()-1).getStartTime());
			
			try {
				PrintWriter out = new PrintWriter("out1.txt");
				out.println(days);
				out.close();
			} catch (FileNotFoundException e1) {
				
				e1.printStackTrace();
			}
			//TASKUL 2
			Map<Activity,Long> mep = mon.stream().collect(Collectors.groupingBy(MonitoredData::getActivityLabel,Collectors.counting()));
		
			try {
				PrintWriter out2 = new PrintWriter("out2.txt");
				
				out2.println(mep);
				out2.close();
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
			//TASK3
			Map<Integer,Map<String,Long>> mep1=mon.stream().collect(Collectors.groupingBy(MonitoredData::getDay,Collectors.groupingBy(MonitoredData::getActivityLabelString,Collectors.counting())));
			
			try {
				PrintWriter out3 = new PrintWriter("out3.txt");
				out3.println(mep1);
				out3.close();
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
			
			Map<String, Long> mep2=mon.stream().collect(Collectors.groupingBy(MonitoredData::getActivityLabelString,Collectors.summingLong(md->md.getDifferenceBetweenTimes()/1000/60/60)));
			Map<String,Long> mep3=mep2.entrySet().stream().filter(val->val.getValue()>10).collect(Collectors.toMap(octav->octav.getKey(), octav->octav.getValue()));
			//task4
			try {
				PrintWriter out4 = new PrintWriter("out4.txt");
				out4.println(mep3);
				out4.close();
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
			Map<Activity,Long> mep5 = mon.stream().filter(p->p.getDifferenceBetweenTimes()/1000/60<5).collect(Collectors.groupingBy(MonitoredData::getActivityLabel,Collectors.counting()));
			List<Activity> list6=new ArrayList<Activity>();
			for(Activity a123:mep5.keySet()){
				if(mep5.get(a123)/mep.get(a123)>=0.9)
					list6.add(a123);
			}
			try {
				PrintWriter out5 = new PrintWriter("out5.txt");
				for(int i=0;i<list6.size();i++){
					out5.println(list6.get(i).toString());
				}
				out5.close();
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
	}
	//receives a string read from the provided text file and returns a representative enum type
	public static Activity getActivity(String s){
		switch (s){
		 case "Leaving": return Activity.Leaving;
		 case "Toileting": return Activity.Toileting;
		 case "Showering": return Activity.Showering;
		 case "Sleeping": return Activity.Sleeping;
		 case "Breakfast": return Activity.Breakfast;
		 case "Lunch": return Activity.Lunch;
		 case "Dinner": return Activity.Dinner;
		 case "Snack": return Activity.Snack;
		 case "Spare_Time/TV": return Activity.Spare_Time;
		 case "Grooming": return Activity.Grooming;
		 default: return null;
		}
	}
	//functional diagram
	public interface Lam{
		Long calculateDays(Date date1,Date date2);
	}
}
